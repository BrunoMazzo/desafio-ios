//
//  RepositoryTableViewCell.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import UIKit
import PINRemoteImage


class RepositoryTableViewCell: UITableViewCell {
    
    let userService: UserService = AlamofireUserService()
    
    var currentUserRequest: Cancelable?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    
    
    override func awakeFromNib() {
        self.prepareForReuse()
    }
    
    override func prepareForReuse() {
        self.titleLabel.text = nil
        self.descriptionLabel.text = nil
        self.starsLabel.text = nil
        self.forksLabel.text = nil
        self.loginLabel.text = nil
        self.userImage.pin_cancelImageDownload()
        self.userNameLabel.text = nil
    }
    
    func setup(repository: Repository) {
        self.titleLabel.text = repository.name
        self.descriptionLabel.text = repository.description
        self.starsLabel.text = "Stars: \(repository.stars)"
        self.forksLabel.text = "Forks: \(repository.forks)"
        self.loginLabel.text = repository.user.login
        self.userImage.pin_setImageFromURL(repository.user.avatarUrl)
     
        self.currentUserRequest?.cancel()
        self.currentUserRequest = self.userService.get(reference: repository.user, completion: { (user, error) in
            self.userNameLabel.text = user?.name ?? user?.company
        })
    }
}