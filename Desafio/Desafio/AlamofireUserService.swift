//
//  AlamofireUserService.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class AlamofireUserService: UserService {
    
    func get(reference reference: UserReference, completion: (User?, NSError?)->()) -> Cancelable {
        
        return Manager.sharedInstance.request(.GET, reference.resourceUrl).responseObject(completionHandler: { (response: Response<User, NSError>) in
            
            if let error = response.result.error {
                completion(nil, error)
                return
            }
            
            if let serverResponse = response.result.value {
                completion(serverResponse, nil)
                return
            }
            
            //Empty response
            let error = NSError(domain: "AlamofireUserService", code: 1, userInfo: nil)
            completion(nil, error)
            
        })
    }
}
