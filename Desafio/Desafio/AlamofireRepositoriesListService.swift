//
//  AlamofireRepositoriesListService.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class AlamofireRepositoriesListService: RepositoriesListService {
    
    func getRepositories(page page: Int, completion: ([Repository]?, NSError?)->()) -> Cancelable {
        
        return Manager.sharedInstance.request(.GET, "https://api.github.com/search/repositories?q=language:Java&sort=stars", parameters: ["page": page]).responseObject(completionHandler: { (response: Response<RepositoryListResponse, NSError>) in
            
            if let error = response.result.error {
                completion(nil, error)
                return
            }
            
            if let serverResponse = response.result.value, items = serverResponse.items where !items.isEmpty {
                completion(serverResponse.items, nil)
                return
            }
            
            //Empty response
            let error = NSError(domain: "AlamofireRepositoriesListService", code: 1, userInfo: nil)
            completion(nil, error)
            
        })
    }
}