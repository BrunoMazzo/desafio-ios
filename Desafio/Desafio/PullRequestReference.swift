//
//  PullRequestReference.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper


struct PullRequestReference: Reference {
    var resourceUrl: NSURL!
}

extension PullRequestReference: Mappable {
    
    static func convertStringToUrl(value: String?) -> NSURL? {
        guard let value = value else {
            return nil
        }
        
        //Removing {/number}
        let url = value.stringByReplacingOccurrencesOfString("{/number}", withString: "", options: .CaseInsensitiveSearch, range: value.startIndex..<value.endIndex)
        
        return NSURL(string: url)
    }
    
    init?(_ map: Map) {
        guard let url = map.JSONDictionary["pulls_url"] as? String, let _ = PullRequestReference.convertStringToUrl(url) else {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        let transform = TransformOf<NSURL, String>(fromJSON: { (value: String?) -> NSURL? in
            
            return PullRequestReference.convertStringToUrl(value)
            }, toJSON: { (value: NSURL?) -> String? in
                return value?.absoluteString
        })
        
        resourceUrl <- (map["pulls_url"], transform)
    }

    
}