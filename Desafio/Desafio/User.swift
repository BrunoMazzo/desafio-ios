//
//  User.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper

struct User {
    var id: Int!
    var name: String?
    var company: String?
    var avatarUrl: NSURL!
    var login: String!
}


extension User: Mappable {
    
    init?(_ map: Map) {
        guard let _ = map.JSONDictionary["id"] as? Int else {
            return nil
        }
        
        let name = map.JSONDictionary["name"] as? String
        let company = map.JSONDictionary["company"] as? String
        
        guard name != nil || company != nil else {
            return nil
        }
        
        
        guard let _ = map.JSONDictionary["login"] as? String else {
            return nil
        }
        
        guard let url = map.JSONDictionary["avatar_url"] as? String, let _ = NSURL(string: url ) else {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        company <- map["company"]
        avatarUrl <- (map["avatar_url"], URLTransform())
        login <- map["login"]
    }
}