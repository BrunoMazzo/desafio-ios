//
//  UserReference.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserReference: Reference {
    var login: String!
    var avatarUrl: NSURL!
    var resourceUrl: NSURL!
}


extension UserReference: Mappable {
    init?(_ map: Map) {
        guard let _ = map.JSONDictionary["login"] as? String else {
            return nil
        }
        
        guard let avatarUrl = map.JSONDictionary["avatar_url"] as? String, let _ = NSURL(string: avatarUrl ) else {
            return nil
        }
        
        guard let url = map.JSONDictionary["url"] as? String, let _ = NSURL(string: url ) else {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        login <- map["login"]
        avatarUrl <- (map["avatar_url"], URLTransform())
        resourceUrl <- (map["url"], URLTransform())
    }
}
