//
//  PullRequestTableViewCell.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/18/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import UIKit
import PINRemoteImage


class PullRequestTableViewCell: UITableViewCell {
    
    let userService: UserService = AlamofireUserService()
    
    var currentUserRequest: Cancelable?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    
    override func awakeFromNib() {
        self.prepareForReuse()
    }
    
    override func prepareForReuse() {
        self.titleLabel.text = nil
        self.descriptionLabel.text = nil
        self.loginLabel.text = nil
        self.userImage.pin_cancelImageDownload()
        self.userNameLabel.text = nil
    }
    
    func setup(pullRequest: PullRequest) {
        self.titleLabel.text = pullRequest.title
        self.descriptionLabel.text = pullRequest.body
        self.loginLabel.text = pullRequest.user.login
        self.userImage.pin_setImageFromURL(pullRequest.user.avatarUrl)
        
        self.currentUserRequest?.cancel()
        self.currentUserRequest = self.userService.get(reference: pullRequest.user, completion: { (user, error) in
            self.userNameLabel.text = user?.name ?? user?.company
        })
    }
}