//
//  ListViewController.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/18/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol ListViewController: class {
    
    associatedtype T
    
    weak var loadingView: UIView! { get set}
    weak var errorView: UIView! { get set}
    weak var tryAgainButton: UIButton! { get set}
    weak var tableView: UITableView! { get set}
    var refreshControl: UIRefreshControl! { get set}
    
    var startLoadingOffset: CGFloat { get set}
    
    var disposeBag: DisposeBag { get set}
    
    func buildCellFor(indexPath: NSIndexPath, item: ListItemType<T>) -> UITableViewCell
    func buildListLoader() -> ListLoader<T>
    func userDidSelectItem(item: T)
}

extension ListViewController {
    func configureList() {
        
        self.refreshControl = UIRefreshControl()
        self.tableView.addSubview(self.refreshControl)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        
        
        self.setupRx()
    }
    
    
    
    func isNearTheBottomEdge(contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
        return  (contentOffset.y + tableView.frame.size.height + startLoadingOffset > tableView.contentSize.height) && tableView.contentSize.height > 0
    }
    
    
    func setupRx() {
        let viewModel = ListViewModel<T>(listService: self.buildListLoader())
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ListItemType<T>>>()
        
        dataSource.configureCell = { [weak self] (dataSource, tableView, indexPath, item) -> UITableViewCell in
            if let `self` = self {
                return self.buildCellFor(indexPath, item: item)
            } else {
                fatalError()
            }
        }
        
        let showLoading = Observable.combineLatest(viewModel.loading.asObservable(), viewModel.items.asObservable()) { (loading, itens) -> Bool in
            return loading && itens.isEmpty
        }
        showLoading.asObservable().map { !$0 }.subscribe(self.loadingView.rx_hidden).addDisposableTo(self.disposeBag)
        
        let showError = Observable.combineLatest(viewModel.loading.asObservable(), viewModel.items.asObservable()) { (loading, itens) -> Bool in
            return !loading && itens.isEmpty
        }
        showError.asObservable().map { !$0 }.subscribe(self.errorView.rx_hidden).addDisposableTo(self.disposeBag)
        
        let showTableView = viewModel.items.asObservable().map { !$0.isEmpty }
        showTableView.asObservable().map { !$0 }.subscribe(self.tableView.rx_hidden).addDisposableTo(self.disposeBag)
        
        
        viewModel.items.asDriver()
            .map { [SectionModel(model: "Items", items: $0.map { ListItemType.item(item: $0) })] }
            .drive(tableView.rx_itemsWithDataSource(dataSource))
            .addDisposableTo(disposeBag)
        
        self.refreshControl.rx_controlEvent(.ValueChanged).subscribeNext { _ in
            viewModel.reload()
            }.addDisposableTo(self.disposeBag)
        
        viewModel.reloading.asDriver().filter { !$0 }.drive(self.refreshControl.rx_refreshing).addDisposableTo(self.disposeBag)
        
        self.tableView.rx_contentOffset
            .flatMap { [weak self] offset -> Observable<Void> in
                if let `self` = self {
                   return self.isNearTheBottomEdge(offset, self.tableView)
                        ? Observable.just()
                        : Observable.empty()
                } else {
                    return Observable.empty()
                }
            }.subscribeNext {
                viewModel.loadMore()
            }.addDisposableTo(self.disposeBag)
        
        self.tableView.rx_itemSelected.asObservable().subscribeNext { [weak self] (index) in
            let item = viewModel.items.value[index.row]
            self?.userDidSelectItem(item)
        }.addDisposableTo(self.disposeBag)
        
        self.tryAgainButton.rx_tap.subscribeNext {
            viewModel.reload()
        }.addDisposableTo(self.disposeBag)
        
        viewModel.begin()
    }
 
}


enum ListItemType<T> {
    case item(item: T)
}

class ListLoader<Type> {
    func getRepositories(page page: Int, completion: ([Type]?, NSError?)->()) -> Cancelable {
        fatalError()
    }
}

class ListViewModel<T> {
    
    let currentPage = Variable<Int?>(nil)
    let items = Variable<[T]>([])
    
    let canLoadMore = Variable<Bool>(true)
    
    let loading: Observable<Bool>
    let loadingMore = ActivityIndicator()
    let reloading = ActivityIndicator()
    
    private let searchNextTrigger = BehaviorSubject<Void?>(value: nil)
    private let reloadTrigger = BehaviorSubject<Void?>(value: nil)
    
    let listService: ListLoader<T>
    
    let disposeBag = DisposeBag()
    
    init(listService: ListLoader<T>) {
        
        self.listService = listService
        
        self.loading = Observable.combineLatest(self.loadingMore.asObservable(), self.reloading.asObservable(), resultSelector: { (isLoadingMore, isReloading) -> Bool in
            return isLoadingMore || isReloading
        }).distinctUntilChanged()
        
        let canLoadMore = Observable.combineLatest(self.canLoadMore.asObservable(), self.loading.asObservable()) { (canLoadMore, isLoadingMore) -> Bool in
            return canLoadMore && !isLoadingMore
        }
        
        let shouldLoadMore = searchNextTrigger.filter { $0 != nil }.asObservable()
            .withLatestFrom(canLoadMore.asObservable())
            .filter { $0 }
        
        shouldLoadMore.map { [weak self] (shouldLoad) -> Observable<([T]?)> in
            if let `self` = self {
                let nextPage = (self.currentPage.value ?? 0) + 1
                return self.request(page: nextPage).retry(3).trackActivity(self.loadingMore).catchErrorJustReturn(nil)
            } else {
                return Observable.empty()
            }
            }.switchLatest().observeOn(MainScheduler.instance).subscribeNext { [weak self] (result) in
                if let `self` = self, result = result {
                    var newValues = self.items.value
                    newValues.appendContentsOf(result)
                    self.items.value = newValues
                    self.canLoadMore.value = result.count == 30
                    self.currentPage.value = (self.currentPage.value ?? 0) + 1
                }
            }.addDisposableTo(self.disposeBag)
        
       self.reloadTrigger.filter { $0 != nil }.flatMapLatest { [weak self] _ -> Observable<([T]?)> in
            if let `self` = self {
                return self.request(page: 1).retry(3).trackActivity(self.reloading).catchErrorJustReturn([])
            } else {
                return Observable.empty()
            }
        }.observeOn(MainScheduler.instance).subscribeNext { [weak self] (result) in
            if let result = result {
                self?.items.value = result
                self?.canLoadMore.value = result.count == 30
                self?.currentPage.value = 1
            }
            }.addDisposableTo(self.disposeBag)
    }
    
    func begin() {
        self.reload()
    }
    
    func reload() {
        self.reloadTrigger.onNext(())
    }
    
    func loadMore() {
        self.searchNextTrigger.onNext(())
    }
    
    func request(page page: Int) -> Observable<([T]?)> {
        return Observable.create({ (observer) -> Disposable in
            let request = self.listService.getRepositories(page: page, completion: { (repository, error) in
                
                if let repository = repository {
                    observer.onNext(repository)
                    observer.onCompleted()
                } else {
                    observer.onError(error ?? NSError(domain: "ListViewModel", code: 1, userInfo: nil))
                }
            })
            
            return AnonymousDisposable {
                request.cancel()
            }
        })
    }
}