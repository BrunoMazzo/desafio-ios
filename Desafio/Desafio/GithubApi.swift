//
//  GithubApi.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation

protocol Cancelable {
    func cancel()
}

protocol RepositoriesListService {
    func getRepositories(page page: Int, completion: ([Repository]?, NSError?)->()) -> Cancelable
}

protocol UserService {
    func get(reference reference: UserReference, completion: (User?, NSError?)->()) -> Cancelable
}

protocol PullRequestListService {
    func get(reference repository: PullRequestReference, page: Int, completion: ([PullRequest]?, NSError?)->()) -> Cancelable
}