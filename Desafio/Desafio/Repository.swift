//
//  Repository.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repository {
    var id: Int!
    var name: String!
    var description: String!
    var user: UserReference!
    var stars: Int!
    var forks: Int!
    var pullRequest: PullRequestReference!
}


extension Repository: Mappable {
    
    init?(_ map: Map) {
        
        guard let _ = map.JSONDictionary["id"] as? Int else {
            return nil
        }
        
        guard let _ = map.JSONDictionary["description"] as? String else {
            return nil
        }
        
        guard let _ = map.JSONDictionary["name"] as? String else {
            return nil
        }
        
        guard let user = map.JSONDictionary["owner"] as? [String: AnyObject], let _ = Mapper<UserReference>().map(user) else {
            return nil
        }
        
        guard let _ = map.JSONDictionary["stargazers_count"] as? Int else {
            return nil
        }
        
        guard let _ = map.JSONDictionary["forks"] as? Int else {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        user <- map["owner"]
        stars <- map["stargazers_count"]
        forks <- map["forks"]
        
        let transform = TransformOf<PullRequestReference, String>(fromJSON: { (value: String?) -> PullRequestReference? in
            guard let value = value else {
                return nil
            }
            return Mapper<PullRequestReference>().map(["pulls_url" : value])
            }, toJSON: { (value: PullRequestReference?) -> String? in
                return value?.resourceUrl.absoluteString
        })
        
        pullRequest <- (map["pulls_url"], transform)
    }
    
}