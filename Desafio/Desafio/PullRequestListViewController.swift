//
//  PullRequestListViewController.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/18/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import UIKit
import RxSwift

class PullRequestListViewController: UIViewController, ListViewController {
    
    typealias T = PullRequest
    
    var pullRequestReference: PullRequestReference?
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var startLoadingOffset: CGFloat = 400
    
    var disposeBag: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureList()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildCellFor(indexPath: NSIndexPath, item: ListItemType<PullRequest>) -> UITableViewCell {
        switch item {
        case .item(item: let pullRequest):
            let cell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.pullRequestCell, forIndexPath: indexPath)
            cell?.setup(pullRequest)
            return cell!
        }
    
    }
    
    func buildListLoader() -> ListLoader<PullRequest> {
        guard let pullRequestReference = self.pullRequestReference else {
            fatalError()
        }
        
        return PullRequestListLoader(reference: pullRequestReference)
    }
    
    func userDidSelectItem(item: PullRequest) {
        UIApplication.sharedApplication().openURL(item.url)
        
        if let selectedPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedPath, animated: true)
        }
    }
}


class PullRequestListLoader: ListLoader<PullRequest> {
    let listService = AlamofirePullRequestListService()
    let reference: PullRequestReference
    
    init(reference: PullRequestReference) {
        self.reference = reference
    }
    
    override func getRepositories(page page: Int, completion: ([PullRequest]?, NSError?)->()) -> Cancelable {
        return listService.get(reference: self.reference, page: page, completion: { pullRequest, error in
            completion(pullRequest, error)
            
        })
    }
}