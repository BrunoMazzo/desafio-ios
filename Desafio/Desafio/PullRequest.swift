//
//  PullRequest.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper

struct PullRequest {
    var user: UserReference!
    var title: String!
    var date: NSDate!
    var body: String!
    var url: NSURL!
}



extension PullRequest: Mappable {
    
    static let dateFormatter = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    
    init?(_ map: Map) {
        guard let _ = map.JSONDictionary["title"] as? String else {
            return nil
        }
        
        guard let _ = map.JSONDictionary["body"] as? String else {
            return nil
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = PullRequest.dateFormatter
        guard let date = map.JSONDictionary["created_at"] as? String, let _ = dateFormatter.dateFromString(date) else {
            return nil
        }
        
        guard let user = map.JSONDictionary["user"] as? [String: AnyObject], let _ = Mapper<UserReference>().map(user) else {
            return nil
        }
        
        guard let url = map.JSONDictionary["html_url"] as? String, let _ = NSURL(string: url ) else {
            return nil
        }
    }
    
    mutating func mapping(map: Map) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = PullRequest.dateFormatter
        user <- map["user"]
        title <- map["title"]
        date <- (map["created_at"], DateFormatterTransform(dateFormatter: dateFormatter))
        body <- map["body"]
        url <- (map["html_url"], URLTransform())
    }
}