//
//  AlamofirePullRequestListService.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class AlamofirePullRequestListService: PullRequestListService {
    
    func get(reference repository: PullRequestReference, page: Int, completion: ([PullRequest]?, NSError?)->()) -> Cancelable {
        
        return Manager.sharedInstance.request(.GET, repository.resourceUrl, parameters: ["page": page]).responseArray(completionHandler: { (response: Response<[PullRequest], NSError>) in
            
            if let error = response.result.error {
                completion(nil, error)
                return
            }
            
            if let serverResponse = response.result.value where !serverResponse.isEmpty {
                completion(serverResponse, nil)
                return
            }
            
            //Empty response
            let error = NSError(domain: "AlamofirePullRequestListService", code: 1, userInfo: nil)
            completion(nil, error)
            
        })
    }
}