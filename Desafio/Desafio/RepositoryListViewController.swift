//
//  ViewController.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class RepositoryListViewController: UIViewController, ListViewController {
    
    typealias T = Repository
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var startLoadingOffset: CGFloat = 400
    
    var disposeBag: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureList()
    }
    
    override func viewWillAppear(animated: Bool) {
        if let selectedPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedPath, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildCellFor(indexPath: NSIndexPath, item: ListItemType<Repository>) -> UITableViewCell {
        switch item {
        case .item(item: let repository):
            let cell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.repositoryCell, forIndexPath: indexPath)
            cell?.setup(repository)
            return cell!
        }

    }
    
    func buildListLoader() -> ListLoader<Repository> {
        return RepositoryListLoader()
    }
    
    var selectedRepository: Repository?
    func userDidSelectItem(item: Repository) {
        self.selectedRepository = item
        self.performSegueWithIdentifier(R.segue.repositoryListViewController.pullRequest, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let pullRequestSegue = R.segue.repositoryListViewController.pullRequest(segue: segue) {
            pullRequestSegue.destinationViewController.pullRequestReference = self.selectedRepository?.pullRequest
        }
    }
    
}

class RepositoryListLoader: ListLoader<Repository> {
    let listService: RepositoriesListService = AlamofireRepositoriesListService()
    
    override func getRepositories(page page: Int, completion: ([Repository]?, NSError?)->()) -> Cancelable {
        return listService.getRepositories(page: page, completion: { (repository, error) in
            completion(repository, error)
        })
    }
}
