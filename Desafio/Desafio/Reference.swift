//
//  Reference.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation

protocol Reference {
    var resourceUrl: NSURL! {get set}
}
