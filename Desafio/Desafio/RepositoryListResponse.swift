//
//  RepositoryListResponse.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import ObjectMapper

struct RepositoryListResponse {
    var items: [Repository]!
}

extension RepositoryListResponse: Mappable {
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        items <- map["items"]
    }
}