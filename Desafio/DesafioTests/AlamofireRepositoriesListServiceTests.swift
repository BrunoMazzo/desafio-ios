//
//  AlamofireRepositoriesListServiceTests.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Mockingjay

@testable import Desafio

class AlamofireRepositoriesListServiceTests: QuickSpec {
    
    var service: AlamofireRepositoriesListService!
    
    override func spec() {
        describe("AlamofireRepositoriesListService") {
            beforeEach {
                self.service = AlamofireRepositoriesListService()
            }
            context("success request") {
                var response: [Repository]? = nil
                var responseError: NSError? = nil
                
                beforeEach {
                    
                    response = nil
                    responseError = nil
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("Repository.List.1", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.stub(uri("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"), builder: jsonData(data))
                }
                
                it("should return an array of repositories") {
                    self.service.getRepositories(page: 1, completion: { (repositories, error) in
                        response = repositories
                        responseError = error
                        
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    expect(response!.count).toEventuallyNot(be(0))
                    expect(responseError).toEventually(beNil())
                }
                
                it("should parse all data") {
                    self.service.getRepositories(page: 1, completion: { (repositories, error) in
                        response = repositories
                        responseError = error
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    for repository in response! {
                        expect(repository.id).toEventuallyNot(beNil())
                        expect(repository.name).toEventuallyNot(beNil())
                        expect(repository.pullRequest).toEventuallyNot(beNil())
                        expect(repository.pullRequest?.resourceUrl).toEventuallyNot(beNil())
                        expect(repository.stars).toEventuallyNot(beNil())
                        expect(repository.user).toEventuallyNot(beNil())
                        expect(repository.user.avatarUrl).toEventuallyNot(beNil())
                        expect(repository.user.login).toEventuallyNot(beNil())
                        expect(repository.user.resourceUrl).toEventuallyNot(beNil())
                        expect(repository.forks).toEventuallyNot(beNil())
                        expect(repository.description).toEventuallyNot(beNil())
                    }
                }
            }
            
            context("success request by invalid data") {
                beforeEach {
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("Repository.List.1.WrongParse", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.stub(uri("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"), builder: jsonData(data))
                }
                
                var response: [Repository]? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.getRepositories(page: 1, completion: { (repositories, error) in
                        response = repositories
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            context("on error") {
                beforeEach {
                    let error = NSError(domain: "Describe your error here", code: 404, userInfo: nil)
                    self.stub(uri("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"), builder: failure(error))
                }
                
                var response: [Repository]? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.getRepositories(page: 1, completion: { (repositories, error) in
                        response = repositories
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            
        }
    }
}
