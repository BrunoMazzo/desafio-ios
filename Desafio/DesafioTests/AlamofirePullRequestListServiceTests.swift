//
//  AlamofirePullRequestListServiceTests.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Mockingjay

@testable import Desafio

class AlamofirePullRequestListServiceTests: QuickSpec {
    
    var service: AlamofirePullRequestListService!
    var pullRequestReference: PullRequestReference!
    
    override func spec() {
        describe("AlamofireRepositoriesListService") {
            beforeEach {
                self.service = AlamofirePullRequestListService()
            }
            context("success request") {
                var response: [PullRequest]? = nil
                var responseError: NSError? = nil
                
                beforeEach {
                    response = nil
                    responseError = nil
                    
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("PullRequest", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.pullRequestReference = PullRequestReference(resourceUrl: NSURL(string: "https://api.github.com/repos/facebook/react-native/pulls"))
                    self.stub(uri("https://api.github.com/repos/facebook/react-native/pulls?page=1"), builder: jsonData(data))
                }
                
                
                
                it("should return an array of pullRequests") {
                    
                    self.service.get(reference: self.pullRequestReference, page: 1, completion: { (pullRequests, error) in
                        response = pullRequests
                        responseError = error
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    expect(response!.count).toEventuallyNot(be(0))
                    expect(responseError).toEventually(beNil())
                    
                }
                
                it("should parse all data") {
                    self.service.get(reference: self.pullRequestReference, page: 1, completion: { (pullRequests, error) in
                        response = pullRequests
                        responseError = error
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    for pullRequest in response! {
                        expect(pullRequest.body).toEventuallyNot(beNil())
                        expect(pullRequest.title).toEventuallyNot(beNil())
                        expect(pullRequest.date).toEventuallyNot(beNil())
                        expect(pullRequest.user).toEventuallyNot(beNil())
                        expect(pullRequest.user.avatarUrl).toEventuallyNot(beNil())
                        expect(pullRequest.user.login).toEventuallyNot(beNil())
                        expect(pullRequest.user.resourceUrl).toEventuallyNot(beNil())
                    }
                }
                
                
            }
            
            context("success request by invalid data") {
                beforeEach {
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("PullRequest.WrongParse", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.pullRequestReference = PullRequestReference(resourceUrl: NSURL(string: "https://api.github.com/repos/facebook/react-native/pulls"))
                    self.stub(uri("https://api.github.com/repos/facebook/react-native/pulls?page=1"), builder: jsonData(data))
                }
                
                var response: [PullRequest]? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.get(reference: self.pullRequestReference, page: 1, completion: { (pullRequests, error) in
                        response = pullRequests
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            context("on error") {
                beforeEach {
                    let error = NSError(domain: "Describe your error here", code: 404, userInfo: nil)
                    self.pullRequestReference = PullRequestReference(resourceUrl: NSURL(string: "https://api.github.com/repos/facebook/react-native/pulls"))
                    self.stub(uri("https://api.github.com/repos/facebook/react-native/pulls?page=1"),  builder: failure(error))
                }
                
                var response: [PullRequest]? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.get(reference: self.pullRequestReference, page: 1, completion: { (pullRequests, error) in
                        response = pullRequests
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            
        }
    }
}
