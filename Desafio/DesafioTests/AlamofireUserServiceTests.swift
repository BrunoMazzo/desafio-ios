//
//  AlamofireUserServiceTests.swift
//  Desafio
//
//  Created by Bruno Mazzo on 9/17/16.
//  Copyright © 2016 Bruno Mazzo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Mockingjay

@testable import Desafio

class AlamofireUserServiceTests: QuickSpec {
    
    var service: AlamofireUserService!
    var userReference: UserReference!
    
    override func spec() {
        describe("AlamofireRepositoriesListService") {
            beforeEach {
                self.service = AlamofireUserService()
            }
            context("success request") {
                var response: User? = nil
                var responseError: NSError? = nil
                
                beforeEach {
                    response = nil
                    responseError = nil
                    
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("User", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.userReference = UserReference(login: "Login", avatarUrl: NSURL(string: "https://avatars.githubusercontent.com/u/69631?v=3"), resourceUrl: NSURL(string: "https://api.github.com/users/facebook"))
                    
                    self.stub(uri("https://api.github.com/users/facebook"), builder: jsonData(data))
                }
                
                
                
                it("should return an user") {
                    
                    self.service.get(reference: self.userReference, completion: { (user, error) in
                        response = user
                        responseError = error
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    expect(responseError).toEventually(beNil())
                    
                }
                
                it("should parse all data") {
                    self.service.get(reference: self.userReference, completion: { (user, error) in
                        response = user
                        responseError = error
                    })
                    
                    expect(response).toEventuallyNot(beNil())
                    expect(response!.avatarUrl).toEventuallyNot(beNil())
                    expect(response!.id).toEventuallyNot(beNil())
                    expect(response!.login).toEventuallyNot(beNil())
                    expect(response!.name).toEventuallyNot(beNil())
                    
                }
            }
            
            context("success request by invalid data") {
                beforeEach {
                    
                    let path = NSBundle(forClass: self.dynamicType).pathForResource("User.WrongParse", ofType: "json")!
                    
                    let data = NSData(contentsOfFile: path)!
                    
                    self.userReference = UserReference(login: "Login", avatarUrl: NSURL(string: "https://avatars.githubusercontent.com/u/69631?v=3"), resourceUrl: NSURL(string: "https://api.github.com/users/facebook"))
                    
                    self.stub(uri("https://api.github.com/users/facebook"), builder: jsonData(data))
                }
                
                var response: User? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.get(reference: self.userReference, completion: { (user, error) in
                        response = user
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            context("on error") {
                beforeEach {
                    let error = NSError(domain: "Describe your error here", code: 404, userInfo: nil)
                    self.userReference = UserReference(login: "Login", avatarUrl: NSURL(string: "https://avatars.githubusercontent.com/u/69631?v=3"), resourceUrl: NSURL(string: "https://api.github.com/users/facebook"))
                    
                    self.stub(uri("https://api.github.com/users/facebook"), builder: failure(error))
                }
                
                var response: User? = nil
                var responseError: NSError? = nil
                
                it("should return error") {
                    self.service.get(reference: self.userReference, completion: { (user, error) in
                        response = user
                        responseError = error
                    })
                    
                    expect(response).toEventually(beNil())
                    expect(responseError).toEventuallyNot(beNil())
                }
            }
            
            
        }
    }
}
